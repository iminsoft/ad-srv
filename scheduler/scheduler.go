package scheduler

import (
	"time"
)

type (
	Handler func(Parameters)

	Scheduler interface {
		Register(d time.Duration, name string, t Handler)
		Execute(name string, p ...Parameter)
		Run()
	}

	defaultScheduler struct {
		handlers map[Parameters]Handler
	}
)

func (d *defaultScheduler) Run() {
	for pa, ha := range d.handlers {
		go func(p Parameters, h Handler) {
			for range time.NewTicker(p.Frequency).C {
				h(p)
			}
		}(pa, ha)
	}
}

func (d *defaultScheduler) Register(t time.Duration, n string, h Handler) {
	d.handlers[Parameters{
		Name:      n,
		Frequency: t,
	}] = h
}

func (d *defaultScheduler) Execute(name string, parameters ...Parameter) {
	for p, h := range d.handlers {
		if p.Name == name {
			for _, o := range parameters {
				o(&p)
			}

			h(p)
		}
	}
}

func NewScheduler() Scheduler {
	return &defaultScheduler{
		handlers: make(map[Parameters]Handler),
	}
}
