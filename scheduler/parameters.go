package scheduler

import "time"

type (
	Parameter func(*Parameters)

	Parameters struct {
		Name      string
		Frequency time.Duration
	}
)

// Frequency of Handler calling
func Frequency(f time.Duration) Parameter {
	return func(p *Parameters) {
		p.Frequency = f
	}
}
