package handler

import (
	. "bitbucket.org/iminsoft/ad-srv/model"
	"bitbucket.org/iminsoft/ad-srv/publisher"
	"bitbucket.org/iminsoft/ad-srv/service"
	"github.com/micro/go-micro/server"
	"golang.org/x/net/context"
	"github.com/sokool/goldap"
)

type Record struct {
	service service.Query
}

func (r *Record) Search(ctx context.Context, req *SearchRequest, res *SearchResponse) error {
	res.Records = r.service.FindByAttribute(req.Attributes)

	res.Records.Each(func(i int, e *ldap.Element) {
		publisher.Send("ad.record", e)
	})

	return nil
}

func (r *Record) Modify(ctx context.Context, req *ModifyRequest, res *ModifyResponse) error {
	return r.service.UpdateAttributes(req.DN, req.Attributes)
}

func Register(s server.Server) {
	s.Handle(s.NewHandler(&Record{
		service: service.New(),
	}))
}
