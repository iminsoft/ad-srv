package main

import (
	"log"

	"bitbucket.org/iminsoft/ad-srv/handler"
	"bitbucket.org/iminsoft/ad-srv/publisher"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/sokool/goldap"
)

func main() {
	service := micro.NewService(
		micro.Name("im.srv.ad"),
		micro.Flags(
			cli.StringFlag{
				Name:  "frequency, f",
				Usage: "Frequency of LDAP queries in order to send user events",
			},
			cli.BoolFlag{
				Name:  "init, i",
				Usage: "",
			},
			cli.StringFlag{
				Name:   "ad_url, u",
				EnvVar: "ACTIVE_DIRECTORY_URL",
				Usage:  "<username>:<password>@<repository>:<port>",
			},
		),
		micro.Action(func(c *cli.Context) {
			if c.IsSet("f") {
				publisher.Frequency = c.String("f")
			}

			if c.IsSet("i") {
				publisher.PublishAll = true
			}

			if len(c.String("u")) > 0 {
				ldap.Url = c.String("u")
			}
		}),
	)

	service.Init()
	publisher.Init()
	handler.Register(service.Server())

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
