package publisher

import (
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/iminsoft/ad-srv/scheduler"
	"bitbucket.org/iminsoft/ad-srv/service"
	"github.com/micro/go-micro/broker"
	"github.com/sokool/goldap"
)

const (
	eventName = "ad.record"
)

var (
	userSer service.Query
	//eventCli event.Event
	schedulerCli scheduler.Scheduler

	Frequency  string = "1h"
	PublishAll bool   = false
)

func Init() {

	if err := broker.Init(); err != nil {
		log.Fatalf("Broker Init error: %v", err)
	}

	if err := broker.Connect(); err != nil {
		log.Fatalf("Broker Connect error: %v", err)
	}

	userSer = service.New()
	//eventCli = event.NewEvent()
	schedulerCli = scheduler.NewScheduler()

	f, err := time.ParseDuration(Frequency)
	if err != nil {
		panic(err)
	}
	log.Printf("User lookup is scheduled every %s", f)

	// Take all users from beginning of company and send them out
	if PublishAll {
		beginning, _ := time.Parse(time.UnixDate, "Sat Mar 7 11:06:39 PST 2011")
		publishUsers(beginning)
	}

	// Send all users, from time defined in Duration parameter
	schedulerCli.Register(f, eventName, func(p scheduler.Parameters) {
		//2h of diff on LDAP server
		then := time.Now().Add(p.Frequency * -1)
		t := then.Add((time.Hour * 2) * -1)
		publishUsers(t)
	})

	schedulerCli.Run()

}

func publishUsers(after time.Time) {
	userSer.FindAfter(after).Each(func(i int, e *ldap.Element) {
		Send(eventName, e)
	})
}

func Send(topic string, data interface{}) {
	var err error
	var j []byte

	j, err = json.Marshal(data)
	if err != nil {
		log.Printf("Event.FAILED: can not convert to json")
		return
	}

	m := &broker.Message{
		Body: j,
	}

	if err = broker.Publish(topic, m); err != nil {
		log.Printf("Event.FAILED: %s\n", err)
	}

}
