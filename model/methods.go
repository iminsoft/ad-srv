package model

import "github.com/sokool/goldap"

type (
	SearchRequest struct {
		Attributes map[string]string `json:"Attributes,omitempty"`
	}

	SearchResponse struct {
		Records *ldap.Result `json:"records, omitempty"`
	}

	ModifyRequest struct {
		DN         string              `json:"dn, omitempty"`
		Attributes map[string][]string `json:"Attributes,omitempty"`
	}

	ModifyResponse struct {
	}
)

func (m *SearchRequest) Reset() {
	*m = SearchRequest{}
}

func (m *SearchResponse) Reset() {
	*m = SearchResponse{}
}
