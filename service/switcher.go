package service

import (
	b64 "encoding/base64"
	"encoding/hex"
	"strconv"
	"time"
)

var (
	filters = map[string]func(string) string{
		"ztot":      zuluToTime,
		"b64decode": base64decode,
		"htos":      hexToString,
	}
)

type (
	Transferer interface {
		Transfer(a Attributes)
	}

	Switcher struct {
		From    []string
		To      string
		Filters []string
	}
)

func Mapper(s []Switcher, a Attributes, t Transferer) {
	data := Attributes{}
	for _, sw := range s {
		for _, f := range sw.From {
			values, ok := a.get(f)
			if !ok {
				continue
			}

			var out []string
			for _, v := range values {
				out = append(out, sw.filter(v))
			}

			data[sw.To] = out
		}
	}
	t.Transfer(data)
}

func (m *Switcher) filter(v string) string {
	for _, fi := range m.Filters {
		if f, is := filters[fi]; is {
			v = f(v)
		}
	}
	return v
}

func zuluToTime(z string) string {

	location, _ := time.LoadLocation("Local")
	year, _ := strconv.Atoi(z[:4])
	month, _ := strconv.Atoi(z[4:6])
	day, _ := strconv.Atoi(z[6:8])
	hour, _ := strconv.Atoi(z[8:10])
	minute, _ := strconv.Atoi(z[10:12])
	second, _ := strconv.Atoi(z[12:14])

	return time.Date(year, time.Month(month), day, hour, minute, second, 0, location).Format(time.UnixDate)
}

func base64decode(s string) string {
	o, _ := b64.StdEncoding.DecodeString(s)

	return string(o)
}

func hexToString(s string) string {
	return hex.EncodeToString([]byte(s))
}
