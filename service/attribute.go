package service

import (
	"encoding/json"
	"fmt"
)

type (
	Attributes map[string][]string
)

func (a Attributes) First(name string) string {
	if s, ok := a.get(name); ok {
		return s[0]
	}

	return ""
}

func (a Attributes) All(name string) []string {
	if s, ok := a.get(name); ok {
		return s
	}
	return nil
}

func (a Attributes) Add(name string, v []string) {
	a[name] = v
}

func (a Attributes) get(name string) ([]string, bool) {
	if a, is := a[name]; is {
		return a, true
	}

	return nil, false
}

func NewAttributes(s string) (Attributes, error) {
	var a Attributes
	err := json.Unmarshal([]byte(s), &a)
	if err != nil {
		return nil, fmt.Errorf("Given string is not JSON valid\n")
	}

	return a, nil

}
