package service

import (
	"time"

	"github.com/sokool/goldap"
	"github.com/sokool/goldap/filter"
	"github.com/sokool/goldap/sanitizer"
)

var (
	base64 = []string{
		"objectGUID", "objectSid", "msExchBlockedSendersHash", "msExchSafeSendersHash", "logonHours",
		"userCertificate", "msExchSafeRecipientsHash", "photo", "thumbnailPhoto", "mSMQSignCertificates", "mSMQDigests",
	}

	jsonAttributes = []string{"*"}
	jsonEncoding   = map[string][]string{"base64encode": base64}
)

type (
	Query interface {
		FindAfter(after time.Time) *ldap.Result
		FindByAttribute(attributes map[string]string) *ldap.Result
		UpdateAttributes(dn string, attributes map[string][]string) error
	}

	defaultQuery struct {
		conn *ldap.LDAP
	}
)

func (d *defaultQuery) FindAfter(after time.Time) *ldap.Result {
	return d.conn.Search(jsonAttributes, jsonEncoding).When(filter.AND(
		filter.Equal("objectClass", "person"),
		filter.NOT(
			filter.Equal("objectClass", "computer"),
		),
		filter.Gte("whenChanged", sanitizer.TimeToZulu(after)),
	)).Fetch()
}

func (d *defaultQuery) FindByAttribute(attributes map[string]string) *ldap.Result {
	var f filter.Filter

	for n, v := range attributes {
		f = f + filter.Equal(n, v)
	}

	return d.conn.
		Search(jsonAttributes, jsonEncoding).
		When(filter.AND(f)).
		Fetch()
}

func (d *defaultQuery) UpdateAttributes(dn string, attributes map[string][]string) error {
	return d.conn.
		Modify(map[string][]string{"base64decode": base64}).
		In(dn).
		What(attributes).
		Flush()

}

func New() Query {
	return &defaultQuery{
		conn: ldap.New(),
	}
}
